import scala.annotation.tailrec

object GuessTheWord {

  case class GameState(letters: Seq[Letter], guesses: Int = 10)
  case class Letter(character: Char, guessed: Boolean = false)

  def main(args: Array[String]): Unit = {
    val word = WordGen.randWord()
    val initialState = new GameState(strToLetterList(word), 10)

    val didWin = round(initialState)

    if (didWin)
      println("You win!")
    else
      println("You lose!")
  }

  @tailrec def round(state: GameState): Boolean = {
    printState(state)
    if (didWin(state))
      true
    else if (didLose(state))
      false
    else {
      println("Guess a letter")
      val line = readLine
      val c = getChar(line)
      if (c == None) {
        println("Invalid input")
        round(state)
      } else
        round(guess(state, c.get))
    }
  }

  def printState(state: GameState): Unit = {
    println("%s   Guesses: %d".format(hiddenForm(state.letters), state.guesses))
  }

  def strToLetterList(str: String): Seq[Letter] = str map (new Letter(_))

  def visibleForm(letters: Seq[Letter]) = letters map (_.character) mkString

  def hiddenForm(letters: Seq[Letter]) = {
    letters map (letter =>
      if (letter.guessed)
        letter.character
      else
        '_'
      ) mkString
  }

  def didWin(state: GameState): Boolean = state.letters forall (_.guessed)

  def didLose(state: GameState): Boolean = state.guesses == 0

  def getChar(line: String): Option[Char] = {
    if ((line isEmpty) || line.length() > 1)
      None
    else
      Some(line(0))
  }

  def guess(state: GameState, c: Char): GameState = {
    val letters = state.letters

    val updatedLetters = letters map (letter =>
      if (letter.character == c)
        new Letter(letter.character, true)
      else
        new Letter(letter.character, letter.guessed)
      )

     new GameState(updatedLetters, state.guesses - 1)
  }
}