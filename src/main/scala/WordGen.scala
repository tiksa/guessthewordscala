import java.io.InputStream

import scala.util.Random

object WordGen {

  val stream : InputStream = getClass.getResourceAsStream("/dict.txt")
  val lines = IndexedSeq() ++ scala.io.Source.fromInputStream( stream ).getLines

  def randWord(): String = {
    val rnd = new Random
    lines(rnd.nextInt(lines.length))
  }
}
